import unittest

from blue_yon_task import download


class TestDownload(unittest.TestCase):

    
    # test for failing to open an invalid file
    def test_invalidfile(self):

        # call the download function with non-existent filename as argument
        value = download('urls2.txt')

        # value should be equal to false
        self.assertEquals(False, value)

    # test for sucessfully opening a valid file
    def test_validfile(self):

        # call the download function with an existent filename as argument
        value = download('urls.txt')

        # value should not be equal to false as the file is valid
        self.assertNotEquals(False, value)

    # test for validating the successfull & unsuccessfull download

    def test_download(self):

        # call the download function with an existent filename as argument
        value = download('urls.txt')

        # the file text.txt contains two valid urls and one invalid
        # so the download function should return a list [2,1]
        self.assertEquals([2, 1], value)


if __name__ == '__main__':
    unittest.main()
