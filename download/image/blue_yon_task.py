import urllib.request
import os
from pathlib import Path


# function to download objects from the specified urls in the text file
def download(fileName):

    # forming the path for the file to be read
    dirCurr = str(Path().absolute())
    file = os.path.join(dirCurr, "text_files", fileName)

    # Openfile flag determines if contents of text file will be read
    # Openfile flag is set to true by default. It changes to False
    # if the file could not be opened
    openFile = True
    # read from text file into list named urlLines
    try:
        with open(file) as f:
            urlLines = f.readlines()

    except Exception as e:

        print("unable to read file due to eception", e)
        openFile = False
        return openFile

    # continue working on the file only if the file was successfully opened

    trim_url(urlLines)

    # print(urlLines)

    return save_content(urlLines)


# the function trimUrl removes the appended characters '/n' from the urls in the
# list named urlLines
def trim_url(urlList):
    for i, x in enumerate(urlList):

        length = len(x)

        # list named urlLines contains the url strings with '/n' appended in the end
        # inherently by python to facilitate for a new line after each url
        # slice the url strings remove last 2 characters (/n)
        new_x = x[:length - 1]
        # print(new_x)
        urlList[i] = new_x


# this function performs the download and saving of data from the urls
def save_content(urlList):
    # download the images from the urls in the list

    downloadFail = 0  # indicates nr of times download failed
    filesDownloaded = 0  # indicates nr of times files were downloaded

    for x in urlList:

        # Downloaded flag determines if files will be saved locally
        # Downloaded flag is set to true by default. It changes to False
        # if the download was unsuccessfull
        downloaded = True
        url = x

        try:
            # retreive the contents from the url
            data = urllib.request.urlopen(url,)
            # print(conn.read())

        except Exception as e:
            print("unable to download due to eception", e, "at", url)

            # set the downloaded flag to false
            downloaded = False
            downloadFail += 1

        # save only if download was succesful
        if downloaded == True:

            filesDownloaded += 1
            # the filename for the downloaded files will be same as their name in the url
            # extracting the file names from the url
            fileName = url.split("/")[-1]

            dirCurr = str(Path().absolute())
            filePath = os.path.join(dirCurr, "img_files", fileName)
            # open the file on the local drive in the current directory to be written
            # in binary mode 'wb' (for img files)
            file = open(filePath, 'wb')

            # writing the image bytes retreived from the url
            file.write(data.read())

            # closing the file
            file.close()

    # keeping track of nr of downloaded files and nr of failed download in fileMetrics
    # to facilitate unit testing
    fileMetrics = [filesDownloaded, downloadFail]

    return (fileMetrics)


# call the download funtion
download('urls.txt')
